variables:
  default_lava_tests: "--test-case ltp-smoketest.yaml"
  KERNEL_REPO_NAME: $CI_PROJECT_NAME
  KERNEL_BRANCH: $CI_COMMIT_REF_NAME
  KERNEL_SHA: $CI_COMMIT_SHA
  KERNEL_REPO_URL: $CI_PROJECT_URL
  QA_SERVER: "https://qa-reports.linaro.org"
  LAVA_SERVER: "https://lkft.validation.linaro.org/RPC2/"
  GIT_STRATEGY: none

  # This will be the SQUAD project slug and name like the
  # https://qa-reports.linaro.org/lkft/

  # QA_PROJECT is the unique identifier for a project.
  QA_PROJECT: $CI_PROJECT_NAME-$CI_COMMIT_REF_NAME

  # QA_PROJECT_NAME is the name connected to the QA_PROJECT identifier.
  QA_PROJECT_NAME: "$CI_PROJECT_NAME $CI_COMMIT_REF_NAME on OE"

  # its the same with the sanity jobs.
  QA_PROJECT_SANITY: $CI_PROJECT_NAME-$CI_COMMIT_REF_NAME-sanity
  QA_PROJECT_NAME_SANITY: "$CI_PROJECT_NAME $CI_COMMIT_REF_NAME on OE - sanity"

  # To configure and run sanity tests on real HW or QEMU.
  # Sanity jobs will just run LTP's smoke test suite.
  # If you want to run the sanity jobs you need to set this variable to the
  # string "1".
  RUN_SANITY: "0"

  # To configure and run tests on real HW or QEMU.
  # If you want to run the test jobs you need to set this variable to the
  # string "1".
  RUN_TEST: "0"

  # To configure and run bisection builds when a kernel build fails.
  # If you want to run the bisection jobs you need to set this variable to the
  # string "1".
  RUN_AUTOMATED_BUILD_BISECTION: "0"

  # To configure how many steps it will jump to find the latest GOOD
  # sha. BISECT_COUNT * 2 for every iteration.
  BISECT_COUNT: "32"

  # When bisecting you may want to set the previous good sha/branch to bisect
  # against. If you don't want to going back trying to find the latest good
  # with BISECT_COUNT * 2, set LATEST_GOOD.
  # LATEST_GOOD: "sha"

  # To have the complete (non-sanity) tests as a manual step.
  # With this, it's possible to run selective tests via a click on the pipeline
  # or via an API call.
  MANUAL_TEST: "0"

  # To run a test job with an older/newer DTB than the kernel, for DTB ABI
  # testing. With this, it's possible to run tests with a dtb from another
  # kernel release than the kernel.
  # passed in to the lava-test-plans. Default EXT_DTB_URL==the kernel's
  # tuxsuite's artifact url for the kernel we want to test.

  # To run a test job with kernel tools
  # With this, it's possible to run selective tests with kernel tools url
  # passed in to the lava-test-plans. Default KERNEL_TOOLS_URL==the kernel's
  # tuxsuite's artifact url for the kernel we want to test.
  # USE_EXT_KERNEL_TOOLS: "0"

  # To run a test and/or sanity job without modules
  # With this, it's possible to run selective tests with no modules
  # USE_MODULES: "0"

  # This for determine which test we already know fails or not.  Use the default
  # for mainline or next, use the released branch name for released kernel's
  # e.g., 'linux-5.8.y'.
  SKIPGEN_KERNEL_VERSION: "default"

  # Needs to be set by the user:

  # This is the build configuration file for TuxBuild.
  tuxconfig: ""

  # This is the SQUAD "group/user space" name https://qa-reports.linaro.org/
  # Example: LKFT is the "group" name and "~anders.roxell" is a "user space"
  # name.
  QA_TEAM: ""

  # The time to wait in seconds before checking the first time if all jobs are
  # done so a test report can be sent or not.
  QA_WAIT_BEFORE_NOTIFICATION_TIMEOUT: 600

  # The time to wait in seconds until we send out a test report even if all
  # jobs isn't finished. If there is some maximum time before we don't care
  # anymore about the jobs since they took too long time.
  QA_NOTIFICATION_TIMEOUT: 28800

  # This variable should be set to 'True' if the build should be finished after
  # the QA_NOTIFICATION_TIMEOUT expires.
  QA_FORCE_FINISHING_BUILDS_ON_TIMEOUT: "false"

  # Review email default from address
  REVIEW_FROM: "lkft@linaro.org"

  # Review email default to address
  REVIEW_TO: ""

  # Review email default cc addresses
  REVIEW_CC: ""

  # Configuration file for creating reports with squad-report
  SQUAD_REPORT_CONFIG: ""

  # Set this variable if you want a different report than the default. e.g., SQUAD_REPORTS: "report report-full"
  SQUAD_REPORTS: "report"

  # set extra variables to SQUAD report like --unfinished
  SQUAD_REPORT_EXTRA_OPTIONS: "--unfinished"

  # Validate that tags from the given tree are signed by any of the following
  # GPG keys (separated by space). GPG key in (long) fingerprint format. E.g.:
  # for GregKH's key:
  #   GPG_KEY_FINGERPRINTS: "647F28654894E3BD457199BE38DBBDC86092693E"
  GPG_KEY_FINGERPRINTS: ""

  # Define a Tuxplan location for the storage of the root file systems to be
  # used by the tests. These location must include all the needed images for
  # the tests to be executed.
  # If unset, use the predefined ROOTFS_DEPOT from gen-variables.sh.
  ROOTFS_TUXPLAN_ID: "2LM5mhEbbl98CgYSjKoyCyWVXdV"

  # Default Tuxsuite project for Tuxplans. This can be overridden as per user's
  # choice with a different Tuxsuite project. Changing that here will yield a
  # different URL for the downloads.
  TUXSUITE_PROJECT: "katie"

validate_inputs:
  image: debian:bullseye-slim
  stage: prerequisites
  script:
    - echo "Checking for required parameters"
    - 'echo "KERNEL_REPO_NAME: ${KERNEL_REPO_NAME}"'
    - 'echo "KERNEL_BRANCH: ${KERNEL_BRANCH}"'
    - 'echo "KERNEL_SHA: ${KERNEL_SHA}"'
    - 'echo "KERNEL_REPO_URL: ${KERNEL_REPO_URL}"'
    - 'if [[ "${KERNEL_REPO_URL}" = "^https*" ]]; then echo "Error: KERNEL_REPO_URL must be in the format of https://."; exit 1; fi'
    - test -n "${KERNEL_BRANCH}"
    - test -n "${KERNEL_SHA}"
    - echo -n "${KERNEL_SHA}" | egrep -q "^[a-f0-9]{12,40}$"
    - for v in KERNEL_REPO_NAME KERNEL_BRANCH KERNEL_SHA KERNEL_REPO_URL; do echo -e "${v}=${!v}\n" >> inputs.sh; done
    - ROOTFS_DATE="${DATE:-$(date +%Y%m%d-%H%M%S)}"
    - echo "${ROOTFS_DATE}" > date.txt
  artifacts:
    paths:
      - inputs.sh
      - date.txt
    when: always

check_project:
  image: squadproject/squad-client:latest
  stage: prerequisites
  script:
    - 'export QA_PROJECT=${QA_PROJECT//\//_}'
    - 'export QA_PROJECT_SANITY=${QA_PROJECT_SANITY//\//_}'
    - 'echo "QA_SERVER: ${QA_SERVER}"'
    - 'echo "QA_PROJECT: ${QA_PROJECT}"'
    - 'echo "QA_PROJECT_NAME: ${QA_PROJECT_NAME}"'
    - 'echo "QA_PROJECT_SANITY: ${QA_PROJECT_SANITY}"'
    - 'echo "QA_PROJECT_NAME_SANITY: ${QA_PROJECT_NAME_SANITY}"'
    - test -n "${QA_PROJECT_NAME}" || export QA_PROJECT_NAME="${QA_PROJECT}"
    - if [ "${QA_TEAM}" == "~katie.worton" ]; then ./squad-client-cup.sh ${QA_PROJECT} "${QA_PROJECT_NAME}" ; else echo "SKIP test" ; fi
    - test -n "${QA_PROJECT_NAME_SANITY}" || export QA_PROJECT_NAME="${QA_PROJECT_SANITY}"
    - if [ ! "${RUN_SANITY}" == "0" ]; then ./squad-client-cup.sh ${QA_PROJECT_SANITY} "${QA_PROJECT_NAME_SANITY}" ; else echo "SKIP sanity" ; fi
  needs:
    - job: download_prerequisites
      artifacts: true

verify_tag:
  image: debian:bullseye-slim
  stage: prerequisites
  variables:
    GIT_STRATEGY: clone
    GIT_CHECKOUT: "true"
    GIT_DEPTH: "1"
  script: |
    if [ -z "${GPG_KEY_FINGERPRINTS}" ]; then
      echo "No GPG key to validate. Skipping."
      exit 0
    fi

    apt -q --yes update
    apt -q --yes install --no-install-recommends ca-certificates git gnupg2

    git clone https://git.kernel.org/pub/scm/docs/kernel/pgpkeys.git
    for fingerprint in ${GPG_KEY_FINGERPRINTS}; do
      short_fingerprint="${fingerprint: -16}"
      if [ -e "pgpkeys/keys/${short_fingerprint^^}.asc" ]; then
        gpg2 --import "pgpkeys/keys/${short_fingerprint}.asc"
      else
        echo "GPG key not found in kernel/pgpkeys repository."
        exit 1
      fi
    done

    git tag -v "${CI_COMMIT_TAG}"
  rules:
    - if: '$GPG_KEY_FINGERPRINTS == ""'
      when: never
    - if: '$CI_COMMIT_TAG == null'
      when: never
    - when: on_success

download_prerequisites:
  image: curlimages/curl:latest
  stage: download_prerequisites
  script:
    - curl -fsSOL https://gitlab.com/katiewortonlinaro/common/raw/master/build-afterscript.sh
    - curl -fsSOL https://gitlab.com/katiewortonlinaro/common/raw/master/create-merge-request.sh
    - curl -fsSOL https://gitlab.com/katiewortonlinaro/common/raw/master/gen-report.sh
    - curl -fsSOL https://gitlab.com/katiewortonlinaro/common/raw/master/gen-review.sh
    - curl -fsSOL https://gitlab.com/katiewortonlinaro/common/raw/master/gen-variables.sh
    - curl -fsSOL https://gitlab.com/katiewortonlinaro/common/raw/master/get-artifacts.sh
    - curl -fsSOL https://gitlab.com/katiewortonlinaro/common/raw/master/lib.sh
    - curl -fsSOL https://gitlab.com/katiewortonlinaro/common/raw/master/register-callback.sh
    - curl -fsSOL https://gitlab.com/katiewortonlinaro/common/raw/master/set-oerootfs.sh
    - curl -fsSOL https://gitlab.com/katiewortonlinaro/common/raw/master/squad-client-cup.sh
    - curl -fsSOL https://gitlab.com/katiewortonlinaro/common/raw/master/submit-jobs-to-bisect.sh
    - curl -fsSOL https://gitlab.com/katiewortonlinaro/common/raw/master/tuxoe-build.sh
    - curl -fsSOL https://gitlab.com/Linaro/lkft/rootfs/rootfs-maker/-/raw/master/publish.sh
    - curl -fsSOL https://gitlab.com/Linaro/lkft/rootfs/rootfs-maker/-/raw/master/pull-artifacts.sh
    - chmod +x build-afterscript.sh create-merge-request.sh gen-report.sh gen-review.sh gen-variables.sh get-artifacts.sh register-callback.sh set-oerootfs.sh squad-client-cup.sh submit-jobs-to-bisect.sh tuxoe-build.sh publish.sh pull-artifacts.sh
    - if [ -n "${SQUAD_REPORT_CONFIG}" ]; then curl -fsSLo squad-report-config.yml "$SQUAD_REPORT_CONFIG"; fi
  needs: []
  artifacts:
    paths:
      - build-afterscript.sh
      - create-merge-request.sh
      - gen-report.sh
      - gen-review.sh
      - gen-variables.sh
      - get-artifacts.sh
      - lib.sh
      - register-callback.sh
      - set-oerootfs.sh
      - squad-client-cup.sh
      - squad-report-config.yml
      - submit-jobs-to-bisect.sh
      - tuxoe-build.sh
      - publish.sh
      - pull-artifacts.sh
    when: always

.build-rootfs:
  image: tuxsuite/tuxsuite:latest
  stage: build
  script:
    - echo "building OE rootfs with kernel ${KERNEL_REPO_NAME}-${KERNEL_BRANCH} at revision ${KERNEL_SHA}"
    - export SRCREV="${CI_COMMIT_SHA}"
    - ./tuxoe-build.sh
  artifacts:
    paths:
      - inputs.sh
      - date.txt
      - pub-dest.txt
      - tuxoe-build.json
      - tux.yaml
      - curl_output.json
    when: always
  needs:
    - validate_inputs
    - job: download_prerequisites
      artifacts: true

.build-plan:
  image: tuxsuite/tuxsuite:latest
  stage: build
  script:
    - echo "building ${KERNEL_REPO_NAME}-${KERNEL_BRANCH} at revision ${KERNEL_SHA}"
    - echo tuxsuite plan --git-repo "${CI_PROJECT_URL}" --git-sha "${CI_COMMIT_SHA}" "${tuxconfig}" --json-out "build-plan.json" --no-wait
    - tuxsuite plan --git-repo "${CI_PROJECT_URL}" --git-sha "${CI_COMMIT_SHA}" "${tuxconfig}" --json-out "build-plan.json" --no-wait
    - tuxsuite build --git-repo "${CI_PROJECT_URL}" --git-sha "${CI_COMMIT_SHA}" --target-arch arm64 --toolchain gcc-11 --kconfig tinyconfig --show-logs --json-out "build.json"
    - source lib.sh
    - export git_desc="$(get_git_describe build.json)"
    - pip --quiet --no-cache-dir install squad-client
    - squad-client --squad-token="${QA_REPORTS_TOKEN}" --squad-host="${QA_SERVER}" submit-tuxsuite --group="${QA_TEAM}" --project="${QA_PROJECT}" --build="${git_desc}" --backend tuxsuite.com --json build-plan.json
  artifacts:
    paths:
      - inputs.sh
      - build.json
      - build-plan.json
      - curl_output.json
    when: always
  needs:
    - validate_inputs
    - job: download_prerequisites
      artifacts: true

.build:
  image: tuxsuite/tuxsuite:latest
  stage: build
  script:
    - echo "building ${KERNEL_REPO_NAME}-${KERNEL_BRANCH} at revision ${KERNEL_SHA}"
    - echo tuxsuite plan --git-repo "${CI_PROJECT_URL}" --git-sha "${CI_COMMIT_SHA}" "${tuxconfig}" --job-name "${build_set_name}" --json-out "build.json" --show-logs
    - tuxsuite plan --git-repo "${CI_PROJECT_URL}" --git-sha "${CI_COMMIT_SHA}" "${tuxconfig}" --job-name "${build_set_name}" --json-out "build.json" --show-logs
  after_script:
    - ./build-afterscript.sh
  artifacts:
    paths:
      - inputs.sh
      - build.json
      - curl_output.json
    when: always
  needs:
    - validate_inputs
    - job: download_prerequisites
      artifacts: true

.submit_test:
  image: lavasoftware/lava-test-plans:master
  stage: sanity
  script:
    - export QA_PROJECT=${QA_PROJECT//\//_}
    - echo "Generating and submitting tests for ${ARCH} DEVICE_TYPE ${DEVICE_TYPE}"
    - lava-test-plans --version
    - ./gen-variables.sh
    # The file submit-tests.sh is generated in gen-variables.sh
    - test -f submit-tests.sh && bash submit-tests.sh
    # Register with SQUAD for test completion callback
    - if [[ -v REGISTER_CALLBACK_TOKEN ]]; then ./register-callback.sh; fi
  artifacts:
    paths:
      - submit-tests.sh

.sanity:
  extends:
    - .submit_test
  variables:
    LAVA_JOB_PRIORITY: 80
    LAVA_TESTS: "--test-case ltp-smoketest.yaml"
  before_script:
    - export QA_PROJECT="${QA_PROJECT_SANITY}"
  rules:
    - if: '$RUN_SANITY == "0"'
      when: never
    - when: on_success

.test:
  stage: test
  extends:
    - .submit_test
  rules:
    - if: '$RUN_TEST == "0"'
      when: never
    - if: '$MANUAL_TEST == "1"'
      when: manual
    - when: on_success

# Do not use before_script in this job prototype.
# Instead, reserve it for variable input since nested variables do not work well.
# See: https://docs.gitlab.com/ee/ci/variables/where_variables_can_be_used.html
.report:
  image: squadproject/squad-report:latest
  stage: report
  script:
    - export QA_PROJECT=${QA_PROJECT//\//_}
    - if [ -z "$BUILD_ID" ]; then
    - ./get-artifacts.sh
    - source lib.sh
    - export BUILD_ID="$(get_git_describe build.json)"
    - fi
    - mkdir -p $HOME/.config/squad_report
    - cp squad-report-config.yml $HOME/.config/squad_report/config.yaml
    - for REPORT_TYPE in $(echo $SQUAD_REPORTS); do ./gen-report.sh "$REPORT_TYPE" || true; done
    - squad-find-regressions --group="$QA_TEAM" --project="$QA_PROJECT" --build="$BUILD_ID" > regressions.txt || true
  artifacts:
    paths:
      - artifacts.zip
      - build.json
      - "*.txt"
  needs:
    - job: check_project
    - job: download_prerequisites
      artifacts: true
  rules:
    - when: manual

# Do not use before_script in this job prototype.
# Instead, reserve it for variable input since nested variables do not work well.
# See: https://docs.gitlab.com/ee/ci/variables/where_variables_can_be_used.html
#
# When creating individual review jobs, make sure to need jobs that contain artifacts you require.
#
# needs:
#   - job: check_project
#   - job: download_prerequisites
#     artifacts: true
#   - job: report
#     artifacts: true
#
.review:
  image: squadproject/squad-report:latest
  stage: review
  script:
    - export QA_PROJECT=${QA_PROJECT//\//_}
    - if [ -f "${REPORT_TYPE}.txt"]; then ./gen-review.sh && send-email review.txt ; else echo "skip review for ${REPORT_TYPE}.txt" ; fi
  artifacts:
    paths:
      - review.txt

# Do not use before_script in this job prototype.
# Instead, reserve it for variable input since nested variables do not work well.
# See: https://docs.gitlab.com/ee/ci/variables/where_variables_can_be_used.html
#
# When creating individual upstream jobs, make sure to need jobs that contain artifacts you require.
#
# needs:
#   - job: check_project
#   - job: download_prerequisites
#     artifacts: true
#   - job: report
#     artifacts: true
#
.upstream:
  image: squadproject/squad-report:latest
  stage: upstream
  script:
    - export QA_PROJECT=${QA_PROJECT//\//_}
    - if [ -f "${REPORT_TYPE}.txt" ]; then send-email "${REPORT_TYPE}.txt" ; else echo "skip sending ${REPORT_TYPE}.txt"; fi
  rules:
    - when: manual
