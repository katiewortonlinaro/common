#!/bin/bash

set -euxo pipefail

REPORT_TYPE=${1}

source "lib.sh"

if [ -z "${BUILD_ID}" ]; then
  BUILD_ID="$(get_git_describe build.json)"
fi

suffix="$(get_project_suffix)"

squad-report \
        --url="${QA_SERVER}" \
        --group="$QA_TEAM" \
        --project="$QA_PROJECT" \
        --build="${BUILD_ID}" \
        --email-subject="${QA_TEAM} ${REPORT_TYPE} for ${BUILD_ID}${suffix}" \
        --config-report-type="${REPORT_TYPE}" \
        "${SQUAD_REPORT_EXTRA_OPTIONS}"
