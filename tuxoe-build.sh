#!/bin/bash

set -ex

OE_DISTRO=${OE_DISTRO:-"lkft"}
OE_MACHINE=${OE_MACHINE:-"juno"}
OE_RELEASE=${OE_RELEASE:-"kirkstone"}
OE_IMAGES=${OE_IMAGES:-'"lkft-kselftest-image", "lkft-console-image", "lkft-tux-image"'}
SRCREV=${SRCREV:-"0047d57e6c91177bb731bed5ada6c211868bc27c"}
TUXOE_CONTAINER=${TUXOE_CONTAINER:-"ubuntu-20.04"}
kernel_recipe_version="5.18+git%"

if [ ! -v KERNEL_REPO_NAME ] || [ -z "${KERNEL_REPO_NAME}" ]; then
  echo "Error: KERNEL_REPO_NAME not set correctly,"
  echo "KERNEL_REPO_NAME=${KERNEL_REPO_NAME}"
  exit 1
fi

if [ ! -v KERNEL_BRANCH ] || [ -z "${KERNEL_BRANCH}" ]; then
  echo "Error: KERNEL_BRANCH not set correctly,"
  echo "KERNEL_BRANCH=${KERNEL_BRANCH}"
  exit 1
fi

case "${KERNEL_REPO_NAME}" in
  linux-mainline)
    kernel_recipe="linux-generic-mainline"
    kernel_recipe_version="git%"
    ;;
  linux-next)
    kernel_recipe="linux-generic-next"
    kernel_recipe_version="git%"
    ;;
  linux-stable-rc | linux-stable)
    kernel_recipe="linux-generic-stable-rc"
    [ "${KERNEL_REPO_NAME}" = "linux-stable" ] && kernel_recipe="linux-generic-stable"
    if [ -v KERNEL_BRANCH ]; then
      major_minor="$(echo "${KERNEL_BRANCH}" | sed -e 's#^linux-##' | cut -d\. -f1,2)"
      kernel_recipe_version="${major_minor}+git%"
    fi
    ;;
esac

cat << EOF > tux.yaml
common: &commondata
  "artifacts":
    [
      "images/"
    ]
  "bblayers_conf":
    [
      'BBLAYERS += "../meta-lkft/meta/"',
      'BBLAYERS += "../meta-lkft/meta-lkft-testsuites/"',
      'BBLAYERS += "../meta-openembedded/meta-oe/"',
      'BBLAYERS += "../meta-openembedded/meta-networking/"',
      'BBLAYERS += "../meta-openembedded/meta-python/"',
      'BBLAYERS += "../meta-openembedded/meta-filesystems/"',
      'BBLAYERS += "../meta-clang/"',
      'BBLAYERS += "../meta-arm/meta-arm/"',
      'BBLAYERS += "../meta-arm/meta-arm-bsp/"',
      'BBLAYERS += "../meta-arm/meta-arm-toolchain/"',
      'BBLAYERS += "../meta-ti/meta-ti-bsp/"',
      'BBLAYERS += "../meta-intel/"',
      'BBLAYERS += "../meta-qcom/"',
    ]
  "container": "${TUXOE_CONTAINER}"
  "distro": "${OE_DISTRO}"
  "environment": {}
  "envsetup": "poky/oe-init-build-env"
  "local_conf":
    [
      'PREFERRED_PROVIDER_virtual/kernel = "${kernel_recipe}"',
      'PREFERRED_VERSION_${kernel_recipe} = "${kernel_recipe_version}"',
EOF
for v in ${!SRCREV_@} ${!PREFERRED_@}; do
  echo "      '${v} = \"${!v}\"'," >> tux.yaml
done
cat << EOF >> tux.yaml
      'SRCREV_kernel = "${SRCREV}"'
    ]
  "sources":
    {
      "git_trees":
        [
          {
            "url": "http://git.yoctoproject.org/git/poky",
            "branch": "${OE_RELEASE}",
          },
          {
            "url": "https://github.com/Linaro/meta-lkft",
            "branch": "${OE_RELEASE}",
          },
          {
            "url": "https://github.com/openembedded/meta-openembedded",
            "branch": "${OE_RELEASE}",
          },
          {
            "url": "https://github.com/kraj/meta-clang.git",
            "branch": "${OE_RELEASE}",
          },
          {
            "url": "https://git.yoctoproject.org/git/meta-arm",
            "branch": "${OE_RELEASE}",
          },
          {
            "url": "http://git.yoctoproject.org/git/meta-ti",
            "branch": "${OE_RELEASE}",
          },
          {
            "url": "http://git.yoctoproject.org/git/meta-intel",
            "branch": "${OE_RELEASE}",
          },
          {
            "url": "https://github.com/ndechesne/meta-qcom",
            "branch": "${OE_RELEASE}",
          },
        ],
    }
  "targets": [${OE_IMAGES}]
version: 1
name: Poky LKFT
description: LKFT builds for Poky ${OE_RELEASE}
jobs:
EOF
for machine in \
  am57xx-evm \
  beaglebone \
  dragonboard-410c \
  dragonboard-845c \
  fvp-base \
  genericx86 \
  genericx86-64 \
  intel-core2-32 \
  intel-corei7-64 \
  juno \
  qemuarm64 \
  qemuarm \
  qemuarmv5 \
  qemux86-64 \
  qemux86; do
  # shellcheck disable=SC2015
  echo "${OE_MACHINE}" | tr ',' '\n' | grep -q "^${machine}$" && cat << EOF >> tux.yaml || :
- name: build-${machine}
  bakes:
      - {<<: *commondata, "machine": "${machine}"}
EOF
done

# shellcheck disable=SC2015
echo "${OE_MACHINE}" | tr ',' '\n' | grep -q "^hikey$" && cat << EOF >> tux.yaml || :
- name: build-hikey
  bakes:
      - {
  "artifacts": [
    "images/"
  ],
  "container": "ubuntu-16.04",
  "sources": {
    "repo": {
      "branch": "sumo",
      "manifest": "default.xml",
      "url": "https://gitlab.com/Linaro/lkft/rootfs/lkft-manifest"
    }
  },
  "envsetup": "setup-environment",
  "distro": "lkft",
  "machine": "hikey",
  "target": "rpb-console-image-lkft",
  "environment": {
    "KERNEL_SHA": "17aac9b7af2bc5f7b4426603940e92ae8aa73d5d",
    "SRCREV_kernel": "17aac9b7af2bc5f7b4426603940e92ae8aa73d5d",
    "SRCREV_kernel_hikey": "17aac9b7af2bc5f7b4426603940e92ae8aa73d5d",
    "KERNEL_RECIPE": "linux-generic-stable-rc",
    "KERNEL_BRANCH": "5.15",
    "KERNEL_VERSION": "5.15"
  }
}
EOF

cat tux.yaml

tuxsuite plan tux.yaml --json-out tuxoe-build.json
