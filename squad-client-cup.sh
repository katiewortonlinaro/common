#!/bin/bash

slug=${1}
name=${2}

cat << EOF | tee "run-squad-client-cmd.sh"
#!/bin/bash -e
squad-client --squad-host "${QA_SERVER}" \\
  --squad-token "${QA_REPORTS_TOKEN}" create-or-update-project \\
  --group "${QA_TEAM}" --slug "${slug}" \\
  --is-public --name "${name}" \\
  --plugins linux_log_parser,ltp \\
  --wait-before-notification-timeout "${QA_WAIT_BEFORE_NOTIFICATION_TIMEOUT}" \\
  --notification-timeout "${QA_NOTIFICATION_TIMEOUT}" \\
  --force-finishing-builds-on-timeout "${QA_FORCE_FINISHING_BUILDS_ON_TIMEOUT}" \\
  --important-metadata-keys "build-url,git_ref,git_describe,git_repo,kernel_version" \\
  --thresholds "build/*-warnings" \\
EOF

if [[ -v REGISTER_CALLBACK_TOKEN ]]; then
  cat << EOF | tee -a "run-squad-client-cmd.sh"
  --data-retention 0 \\
  --settings "CALLBACK_HEADERS: {PRIVATE-TOKEN: $REGISTER_CALLBACK_TOKEN}"
EOF
else
  echo "  --data-retention 0" >> run-squad-client-cmd.sh
fi

chmod +x run-squad-client-cmd.sh
./run-squad-client-cmd.sh
